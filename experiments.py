#Import libraries
import numpy as np
#Import pandas to create tables (SARSA and Q-Learning)
import pandas as pd

#create class for the SARSA and Q-Learning
class Experiment(object):
    def __init__(self, action_space, learning_rate =0.01, reward_decay=0.9, epsilon_greedy = 0.9):
        #Assign a list
        self.actions = action_space
        #This will tell when Q values gonna update or not
        self.rate_L = learning_rate

        self.gamma = reward_decay
        self.epsilon = epsilon_greedy

        #Table for the actions
        self.Q_table = pd.DataFrame(columns=self.actions, dtype=np.float64)

    #Create function to choose actions for the agent
    def select_actions(self, results):
        self.check_state(results)
        #Using if statement for the selection of the actions
        if np.random.rand() < self.epsilon:
            #Select most accurate action for the agent
            #.ix supports mixed integer and label based access
            action_for_state = self.Q_table.ix[results, :]
            action_for_state = action_for_state.reindex(np.random.permutation(action_for_state.index))
            action = action_for_state.idxmax()
        else:
            #Else choose random actions
            action = np.random.choice(actions)

        #return
        return action
    #Create function to check whether state exists or not
    def check_state(state):
        #Using if statement to append the new state Table
        if state not in Q_table.index:
            Q_table = Q_table.append(pd.Series([0]*len(actions), index = Q_table.columns, name = state, ))

    def learning(self, *args):
        pass

#Craete QLearning Table
class Q_L_table(Experiment):
    #Initialize
    def __init__(self, actions, learning_rate = 0.01, reward_decay = 0.9, epsilon_greedy = 0.9):
        super(Q_L_table, self).__init__(actions,learning_rate, reward_decay, epsilon_greedy )

    #Create function to check for the goal
    def learning(self, state, action_, reward_, next_state):
        self.check_state(next_state)
        q_table_check = self.Q_table.ix[state, action_]
        #Using if statement
        if next_state != 'goal':
            #next state is not goal
            q_table_target = reward_ + self.gamma * self.Q_table.ix[next_state, :].max()
        else:
            #Next state is goal
            q_table_target = reward_
        #update the table
        self.Q_table.ix[state, action] = self.Q_table.ix[state, action] + self.rate_L * (q_table_target - q_table_check)

#Create function for the SarsaTable
class SARSA_Table(Experiment):
    #Initialize
    def __init__(self,actions, learning_rate = 0.01, reward_decay = 0.9, epsilon_greedy = 0.9):
        super(SARSA_Table, self).__init__(actions, learning_rate, reward_decay, epsilon_greedy)

    #Create function to check for the goal
    def learning(self, state, action_, reward_, next_state,next_action):
        self.check_state(next_state)
        q_table_check = self.Q_table.ix[state, action_]
        #using if statement
        if next_state != 'goal':
            #next state is not goal
            q_target = reward_ + self.gamma * self.Q_table.ix[next_state, next_action]
        else:
            #Next state is goal
            q_table_target = reward_
        self.Q_table.ix[state, action_] = self.Q_table.ix[state, action] + self.rate_L * (q_table_target - q_table_check)
