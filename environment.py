#import libraries
import numpy as np
import time
import sys
#if sys.version_info.major == 2:
    #Standard GUI to represent the grid
import tkinter as tk

#Height of the grid
height_grid = 6
#Width of the grid
width_grid = 6
#pixels for the grid
pixels = 40

#Create class to design grid
class Grid(tk.Tk, object):
    def __init__(self):
        super(Grid, self).__init__()
        self.action_space = ['up', 'down', 'left', 'right']
        #Assign number of actions to given length of the grid for actons
        self.number_of_actions = len(self.action_space)
        self.title('Grid World [6x6]')
        self.geometry('{0}x{1}'.format(height_grid * pixels, height_grid * pixels))
        #Call the function to create grid
        self.design_grid()

    def design_grid(self):
        self.canvas = tk.Canvas(self, bg='white', height = height_grid * pixels, width = width_grid * pixels)

        #for loop to create columns and rows for the grid
        for column in range(0, width_grid * pixels, pixels):
            x0_axis, y0_axis, x1_axis, y1_axis = column, 0 , column, height_grid*pixels
            #create columna\s
            self.canvas.create_line(x0_axis, y0_axis, x1_axis, y1_axis)
        for row in range(0, height_grid * pixels, pixels):
            x0_axis, y0_axis, x1_axis, y1_axis = 0, row, height_grid * pixels, row
            #create rows
            self.canvas.create_line(x0_axis, y0_axis, x1_axis, y1_axis)

        #Assign the starting point of the grid
        starting_point = np.array([20, 20])

        #Create elements in grids
        #pits
        #Position of the one pit
        pit1_position = starting_point + np.array([pixels * 2, pixels])
        #Locating the pit position in the grid
        self.pit1 = self.canvas.create_rectangle(pit1_position[0] - 15, pit1_position[1] - 15, pit1_position[0] +15, pit1_position[1] + 15, fill = 'black')

        #Position of the second pit
        pit2_position = starting_point + np.array([pixels, pixels * 2])
        #Locating the pit position in the grid
        self.pit2 = self.canvas.create_rectangle(pit2_position[0] - 15, pit2_position[1] - 15, pit2_position[0] +15, pit2_position[1] + 15, fill = 'black')

        #Create fire
        #Position of the fire
        fire_position = starting_point + pixels*2
        #Locating the pit position in the grid
        self.fire = self.canvas.create_rectangle(fire_position[0] - 15, fire_position[1] - 15, fire_position[0] +15, fire_position[1] + 15, fill = 'yellow')

        #Create agent
        #Position of the agent
        #agent_position starts from the top
        #Locating the agent position in the grid
        self.agent = self.canvas.create_rectangle(starting_point[0] - 15, starting_point[1] - 15, starting_point[0] + 15, starting_point[1] + 15, fill = 'grey')

        #Close the grids
        self.canvas.pack()

        #Create the function which allows to reset the movement of the agent after certain time
        def restart(self):
            update()
            time.sleep(0.10)
            canvas.delete(self.agent)
            #Starts from starting point
            starting_point = np.array([20, 20])
            #Locating the agent position in the grid
            agent = self.canvas.create_agent(starting_point[0] - 15, starting_point[1] - 15, starting_point[0] + 15, starting_point[1] + 15, fill = 'grey')

            #Return the result
            #Back to the starting point
            return canvas.x_y_axis(agent)

        #Create function for the movement of the agent
        def movement(self, action):
            #movement of agent
            #starting state for the agent
            starting_state = canvas.x_y_axis(agent)
            #Actions of the agent i.e [0 , 0]
            baseAction = np.array([0,0])

            #Using if statement for the movement of the agent "'up', 'down', 'left', 'right'"
            #up
            if action == 0:
                if x[1] > pixels:
                    baseAction[1] = baseAction - pixels
            #down
            elif action == 1:
                if x[1] < (height_grid - 1) * pixels:
                    baseAction[1] = baseAction + pixels
            #right
            elif action == 2:
                if x[0] < (width_grid - 1) * pixels:
                    baseAction[0] = baseAction + pixels
            elif action == 3:
                if x[0] > pixels:
                    baseAction[0] = baseAction - pixels

            #movement
            canvas.agent_move(agent, baseAction[0], baseAction[1])

            #Initialize next state for the agent
            next_state = canvas.x_y_axis(agent)

            #Create function for the rewards
            #if next state equals to position of the fire
            if next_state == canvas.x_y_axis(fire):
                reward = 1
                done = True
            elif next_state == [canvas.x_y_axis(pit1), canvas.x_y_axis(pit2)]:
                reward = -1
                done = True
            else:
                reward = 0
                done = False

            #Return
            return next_state, reward, done

        #Create function which allow to update and return
        def return_update():
            time.sleep(0.5)
            update()
