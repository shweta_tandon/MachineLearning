#Import all libraries
from environment import Grid
from experiments import SARSA_Table

#Create a function to update
def update_():
    for episode in range(100):
        #initial results/observations
        results = environment.restart()
        #choose action based on results
        action = Experiment.select_actions(str(results))
        #While statement to take action and move to next observation
        while True:
            #Start new environment
            environment.return_update()
            #Take action and move to next result and get the reward
            next_result, reward, done = environment.movement(action)
            #Select action according to next result
            next_action = Experiment.choose_action(str(results))
            #Experiment get the idea from SARSA
            Experiment.learning(str(results), action, reward, str(next_result), next_action)
            #interchange the results and actions
            results = next_result
            action = next_action

            if done:
                #break while loop when this episode is finished
                break
    #finished
    #Destroy the environment by using inbuild function destroy()
    environment.destroy()

if __name__  == '__main__':
    environment = Grid()
    Experiment = SARSA_Table(actions = list(range(environment.number_of_actions)))
    environment.after(100, update_)
    environment.mainloop()
